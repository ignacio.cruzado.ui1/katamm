
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Scanner;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
/**
 *
 * @author amart
 */
public class principal {

    static int contador = 0;

    public static void main(String[] args) throws IOException {

        //System.out.println("hola mundo");
        ArrayList<String> colores = new ArrayList<String>();
        String color = "";
        while (!color.equalsIgnoreCase("fin")) {
            color = pedirColores();
            colores.add(color);
        }

        int intentos = numeroDeIntentos();
        // System.out.println("INTENTOS ELEGIDOS: "+intentos);
        empezarJuego(intentos,colores);

    }

    public static String pedirColores() throws IOException {
        InputStreamReader flujo = new InputStreamReader(System.in);
        BufferedReader teclado = new BufferedReader(flujo);
        String color;
        System.out.println("INTRODUCE UN COLOR: ");
        color = teclado.readLine();
        while (color.equals("")) {
            System.out.println("ERROR, Introduce un color");
            color = teclado.readLine();
        }
        return color;

    }

    private static int numeroDeIntentos() throws IOException {
        InputStreamReader flujo = new InputStreamReader(System.in);
        BufferedReader teclado = new BufferedReader(flujo);
        int intentos;
        System.out.println("NÚMERO DE INTENTOS CON LOS QUE QUIERES JUGAR: ");
        intentos = Integer.parseInt(teclado.readLine());
        return intentos;
    }

    private static void empezarJuego(int numIntentos, ArrayList colores) {
        int[] codigoCorrecto = new int[colores.size()];

        int[] codigoJugador = new int[5];
        boolean combinacion = false;
        String[] resultado = new String[5];
        int intentos = 0;

        //genera el patron
        generarMasterMind(codigoCorrecto);
        //imprime el menú principal
        menuPrincipal(numIntentos,codigoJugador);

        do {
            intentos += 1;
            //el jugador introduce sus colores
            introducirColoresJugador(codigoJugador);
//contador <= numIntentos && intentos <= numIntento
            comprobar(codigoCorrecto, codigoJugador, resultado);
            if (intentos <= numIntentos) {
                System.out.println("HAS GANADO");
                combinacion = true;
            } else {
                System.out.println("NO LO HAS DESCIFRADO");
                combinacion = false;
                if (intentos >= 12) {
                    System.out.println("HAS PERDIDO");
                    break;
                }

            }

        } while (!combinacion);

    }

    private static void generarMasterMind(int[] codigoCorrecto) {
        generarArrayColores(codigoCorrecto);
        imprimirArrayInsertado(codigoCorrecto);
    }

    private static void menuPrincipal(int numIntentos,int[] codigoJugador) {
        //Creamos el menú de inicio
        System.out.println("MASTERMIND");
        System.out.println("INTENTOS ELEGIDOS: " + numIntentos);
        System.out.println("Colores posibles: rojo, verde,azul, lila, gris y negro");
        System.out.println("Elige"+ codigoJugador.length+" colores");
        System.out.println("---------------------------------------------------------");
    }

//MÉTODO PARA GENERAR EL CODIGO DE COLORES QUE TINENE QUE ADIVINAR
    private static void generarArrayColores(int[] codigoCorrecto) {

        boolean correcto = false;
        int num;

        for (int i = 0; i < codigoCorrecto.length; i++) {
            do {
                num = aleatorio();
                for (int j = 0; j < codigoCorrecto.length; j++) {
                    if (codigoCorrecto[j] == num) {
                        correcto = false;
                        break;
                    } else {
                        correcto = true;
                    }
                }
            } while (!correcto);
            codigoCorrecto[i] = num;

        }

    }
//METODO PARA GENERARR EL NUMERO ALEATORIO

    private static int aleatorio() {
        int aleatorio;
        aleatorio = (int) (Math.random() * (7 - 1) + 1);
        return aleatorio;
    }

    private static void imprimirArrayInsertado(int[] codigoCorrecto) {
        System.out.println("\nlos colores que has elegido son: ");
        for (int i : codigoCorrecto) {
            System.out.println(traducirCodigoColor(i) + "");
        }
        System.out.println("");

    }

    private static void introducirColoresJugador(int[] codigoJugador) {
//pregunta al jugador por el patron
        rellenarArrayJugador(codigoJugador);
        imprimirArrayInsertado(codigoJugador);
    }

    private static void rellenarArrayJugador(int[] codigoJugador) {
        boolean correcto = false;
        String color;

        for (int i = 0; i < codigoJugador.length; i++) {
            System.out.println("Color de la posición " + (i + 1));
            do {
                color = ScannerString();
                int respuesta;
                respuesta = traducirColorCodigo(color);
                if (respuesta == 0) {
                    correcto = false;
                    System.out.println("ERROR, introduzca un color");

                } else {

                    codigoJugador[i] = respuesta;
                    correcto = true;
                }
            } while (!correcto);

        }
    }

    private static String ScannerString() {
        String color = "";
        boolean error = true;
        Scanner sc = new Scanner(System.in);
        do {
            try {
                color = sc.nextLine();
                error = false;
            } catch (Exception e) {
                System.out.println("error" + e.getMessage());
                error = true;
            }
        } while (error);
        return color;
    }

    private static int traducirColorCodigo(String color) {
        switch (color) {
            case "rojo":
                return 1;
            case "verde":
                return 2;
            case "azul":
                return 3;
            case "lila":
                return 4;
            case "gris":
                return 5;
            case "negro":
                return 6;
            default:
                return 0;

        }

    }
//convierte el número a su string

    private static String traducirCodigoColor(int color) {
        switch (color) {
            case 1:
                return "rojo";
            case 2:
                return "verde";
            case 3:
                return "azul";
            case 4:
                return "lila";
            case 5:
                return "gris";
            case 6:
                return "negro";
            default:
                return "ERROR";

        }
    }

    private static void comprobar(int[] codigoCorrecto, int[] codigoJugador, String[] resultado) {
        //FICHA BLANCA: Color existe pero no en esa posición        
        fichaBlanca(codigoCorrecto, codigoJugador, resultado);
        //FICHA NEGRA: Color en esa posición
        fichaNegra(codigoCorrecto, codigoJugador, resultado);
        quitarNulos(resultado);
        imprimirResultados(resultado);
    }

//comprueba el array del usuario y mira si hay clores en el array original.
//si el color existe en alguna posicion llama al Interpretador y le envia el array resultado, la posicion y un valor 1
    private static void fichaBlanca(int[] codigoCorrecto, int[] codigoJugador, String[] resultado) {
        for (int i = 0; i < codigoJugador.length; i++) {
            for (int j = 0; j < codigoCorrecto.length; j++) {
                if (codigoJugador[i] == codigoCorrecto[i]) {
                    Interpretador(resultado, i, 1);
                }

            }

        }
    }
//comprueba si el array del usuario y comprueba el color y la posicion con el original
//si el color coincide en la posicion llama al métdo Interpretador y le pasa un valor 2

    private static void fichaNegra(int[] codigoCorrecto, int[] codigoJugador, String[] resultado) {
        for (int i = 0; i < codigoJugador.length; i++) {

            if (codigoCorrecto[i] == codigoJugador[i]) {
                Interpretador(resultado, i, 2);
            }

        }
    }

    private static void Interpretador(String[] resultado, int i, int res) {
        if (res == 1) {
            Insertar(resultado, i, "FICHA BLANCA");
        } else {
            if (res == 2) {
                Insertar(resultado, i, "FICHA NEGRA");
                contador++;
            } else {
                Insertar(resultado, i, "     ");
            }
        }
    }

    private static void Insertar(String[] resultado, int i, String res) {
        resultado[i] = res;
    }

//Imprime el resultado de la comprobación 
    private static void imprimirResultados(String[] resultado) {
        System.out.println("--------RESULTADO------------");
        for (String s : resultado) {
            System.out.println(s + " | ");
        }
        System.out.println("");
    }

    private static void quitarNulos(String[] resultado) {
        for (int i = 0; i < resultado.length; i++) {
            if (resultado[i] == null) {
                resultado[i] = "++++++++++++++";
            }
        }
    }

}
