# katamm

Kata Master Mind

## Paso 1: Configuración inicial:

Ejecute los siguiente comandos (sustituir TUSUARIO por tu username):

```
ssh-keygen -t rsa
cat ~/.ssh/id_rsa.pub
# => Pegarlo en el campo Key y pulsar AddKey en el profile: https://gitlab.com/-/profile/keys
git config --global user.name "TUSUARIO"
git config --global user.email "TUSUARIO@gmail.com"
```

## Paso 2: Clonado del repositorio público:

Ejecute los siguiente comandos (sustituir TUSUARIO por tu username):

```
git clone git@gitlab.com:ignacio.cruzado.ui1/katamm.git
cd katamm
```

## Paso 3: Ciclo de desarrollo:

Cada cierto tiempo (e.g.. 30 minutos) o cuando acabes una cierta parte del Desarrollo (no tiene por qué ser una iteración: puede ser una Clase, una Función, ...) ejecute los siguiente comandos:

```
git add -A *
git commit -am “descripcion_del_cambio"
git push
```
